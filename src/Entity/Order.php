<?php
/**
 * Created by PhpStorm.
 * User: Paweł Ledóchowski
 * Date: 05.07.18
 * Time: 09:21
 */

namespace Recruitment\Entity;

use Recruitment\Cart\Item;

class Order
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var array
     */
    protected $items;

    /**
     * @var int
     */
    protected $totalPrice;

    /**
     * @var array
     */
    protected $totalPriceGross = array();

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Order
     */
    public function setId(int $id): Order
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param array $items
     * @return Order
     */
    public function setItems(array $items): Order
    {
        $this->items = $items;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalPrice(): int
    {
        return $this->totalPrice;
    }

    /**
     * @param int $totalPrice
     * @return Order
     */
    public function setTotalPrice(int $totalPrice): Order
    {
        $this->totalPrice = $totalPrice;
        return $this;
    }

    /**
     * @return array
     */
    public function getTotalPriceGross(string $tax): int
    {
        return $this->totalPriceGross[$tax];
    }

    /**
     * @param array $totalPriceGross
     * @return Order
     */
    public function setTotalPriceGross(array $totalPriceGross): Order
    {
        $this->totalPriceGross = $totalPriceGross;
        return $this;
    }



    /**
     * @return array
     */
    public function getDataForView():array
    {
        $dataForView = [
            'id'=>$this->getId(),
            'items'=>$this->getItems(),
            'total_price'=>$this->getTotalPrice(),
            'total_price_tax_0'=>$this->getTotalPriceGross('tax_0'),
            'total_price_tax_5'=>$this->getTotalPriceGross('tax_5'),
            'total_price_tax_8'=>$this->getTotalPriceGross('tax_8'),
            'total_price_tax_23'=>$this->getTotalPriceGross('tax_23')
        ];

        return $dataForView;
    }

    /**
     * @param Item $item
     *
     * @return Order
     */
    public function addItem(array $item): Order
    {
        $this->items[] = $item;
        return $this;
    }
}

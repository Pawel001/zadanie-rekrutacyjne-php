<?php
/**
 * Created by PhpStorm.
 * User: Paweł Ledóchowski
 * Date: 05.07.18
 * Time: 09:22
 */

namespace Recruitment\Entity;

use InvalidArgumentException;
use Recruitment\Cart\Exception\InvalidTaxValueException;
use Recruitment\Entity\Exception\InvalidUnitPriceException;

class Product
{
    /**
     * name - was required but it's never used (no getters and setters)
     *
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $unitPrice = 1;

    /**
     * @var int
     */
    protected $minimumQuantity = 1;

    /**
     * @var int
     */
    protected $tax;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Product
     */
    public function setId(int $id): Product
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getUnitPrice(): int
    {
        return $this->unitPrice;
    }

    /**
     * @param int $unitPrice
     *
     * @return Product
     *
     * @throws InvalidUnitPriceException
     */
    public function setUnitPrice(int $unitPrice): Product
    {
        if ($unitPrice <= 0) {
            throw new InvalidUnitPriceException("Invalid Unit Price");
        }
        $this->unitPrice = $unitPrice;
        return $this;
    }

    /**
     * @return int
     *
     * @throws InvalidTaxValueException
     */
    public function getUnitPriceGross(): int
    {
        if (!isset($this->tax) || $this->getTax()<0) {
            throw new InvalidTaxValueException();
        }

        return round($this->getUnitPrice() * (1 + $this->getTax() / 100));
    }

    /**
     * @return int
     */
    public function getTax(): int
    {
        return $this->tax;
    }

    /**
     * @param int $tax
     *
     * @return Product
     */
    public function setTax(int $tax): Product
    {
        $this->tax = $tax;
        return $this;
    }


    /**
     * @return int
     */
    public function getMinimumQuantity(): int
    {
        return $this->minimumQuantity;
    }

    /**
     * @param int $minimumQuantity
     *
     * @return Product
     *
     * @throws InvalidArgumentException
     */
    public function setMinimumQuantity(int $minimumQuantity): Product
    {
        if ($minimumQuantity <= 0) {
            throw new InvalidArgumentException("Quantity too low");
        }
        $this->minimumQuantity = $minimumQuantity;
        return $this;
    }
}

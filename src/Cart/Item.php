<?php
/**
 * Created by PhpStorm.
 * User: Paweł Ledóchowski
 * Date: 05.07.18
 * Time: 09:24
 */

namespace Recruitment\Cart;

use InvalidArgumentException;
use Recruitment\Cart\Exception\InvalidTaxValueException;
use Recruitment\Cart\Exception\QuantityTooLowException;
use Recruitment\Entity\Product;

class Item
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var int
     */
    protected $quantity;

    /**
     * @var int
     */
    protected $totalPrice;


    /**
     * Item constructor.
     *
     * @param Product $product
     * @param int $quantity
     */
    public function __construct(Product $product, int $quantity)
    {
        if ($quantity < $product->getMinimumQuantity()) {
            throw new InvalidArgumentException("Quantity is too low");
        }
        $this->product = $product;
        $this->quantity = $quantity;
        $this->totalPrice = $product->getUnitPrice() * $this->getQuantity();
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     *
     * @return Item
     */
    public function setProduct(Product $product): Item
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     *
     * @return Item
     *
     * @throws QuantityTooLowException
     */
    public function setQuantity(int $quantity): Item
    {
        if ($quantity < $this->getProduct()->getMinimumQuantity()) {
            throw new QuantityTooLowException("Quantity is too low");
        }
        $this->quantity = $quantity;
        $this->setTotalPrice($this->getProduct()->getUnitPrice() * $this->getQuantity());
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalPrice(): int
    {
        return $this->totalPrice;
    }

    /**
     * @param int $totalPrice
     *
     * @return Item
     */
    public function setTotalPrice(int $totalPrice): Item
    {
        $this->totalPrice = $totalPrice;
        return $this;
    }

    /**
     * @param int $tax
     *
     * @return int
     *
     * @throws InvalidTaxValueException
     */
    public function getTotalPriceGross(int $tax): int
    {
        if ($tax < 0) {
            throw new InvalidTaxValueException("Tax can't be lower than zero");
        }
        return $this->getProduct()->setTax($tax)->getUnitPriceGross() * $this->getQuantity();
    }
}

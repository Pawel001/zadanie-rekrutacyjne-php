<?php
/**
 * Created by PhpStorm.
 * User: Paweł Ledóchowski
 * Date: 05.07.18
 * Time: 09:14
 */

namespace Recruitment\Cart;

use OutOfBoundsException;
use Recruitment\Entity\Order;
use Recruitment\Entity\Product;

class Cart
{

    /**
     * Array stores the list of items in the cart:
     *
     * @var array
     */
    protected $items = array();

    /**
     * Integer stores total price of all items in the cart:
     *
     * @var int
     */
    protected $totalPrice;

    /**
     * Array store all tax values:
     *
     * @var array
     */
    protected $taxValues = [
        'tax_0' => 0,
        'tax_5' => 5,
        'tax_8' => 8,
        'tax_23' => 23
    ];

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param array $items
     * @return Cart
     */
    public function setItems(array $items): Cart
    {
        $this->items = $items;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalPrice(): int
    {
        return $this->totalPrice;
    }


    /**
     * @param int $totalPrice
     * @return Cart
     */
    public function setTotalPrice(int $totalPrice): Cart
    {
        $this->totalPrice = $totalPrice;
        return $this;
    }


    /**
     * @param Product $product
     * @param int $quantity
     *
     * @return Cart
     */
    public function addProduct(Product $product, int $quantity = 1): Cart
    {
        foreach ($this->getItems() as $item) {
            if ($item->getProduct() === $product) {
                $item->setQuantity($item->getQuantity() + $quantity);
                $this->updatePrice();

                return $this;
            }
        }

        $item = new Item($product, $quantity);
        $this->addItem($item);
        $this->updatePrice();

        return $this;
    }


    /**
     * @param Product $product
     *
     * @return Cart
     */
    public function removeProduct(Product $product): Cart
    {
        foreach ($this->items as $key => $item) {
            if ($item->getProduct() === $product) {
                array_splice($this->items, $key, 1);
                $this->updatePrice();
                return $this;
            }
        }

        return $this;
    }


    /**
     * Update total price of all the Cart items
     *
     * @return Cart
     */
    public function updatePrice(): Cart
    {
        $total = 0;

        foreach ($this->items as $item) {
            $total += $item->getTotalPrice();
        }

        $this->setTotalPrice($total);

        return $this;
    }


    /**
     * @param Product $product
     * @param int $quantity
     *
     * @return Cart
     */
    public function setQuantity(Product $product, int $quantity): Cart
    {
        foreach ($this->items as $item) {
            if ($item->getProduct() === $product) {
                $item->setQuantity($quantity);

                $this->updatePrice();

                return $this;
            }
        }

        return $this->addProduct($product, $quantity);

    }

    /**
     * @param int $index
     *
     * @return Item
     */
    public function getItem(int $index): Item
    {
        if (!isset($this->items[$index])) {
            throw new OutOfBoundsException("Index dose not exist");
        }
        return $this->items[$index];
    }

    /**
     * Transfer data from Cart to Order and clean up the Cart
     *
     * @param int $id
     *
     * @return Order
     */
    public function checkout(int $id): Order
    {
        $order = new Order();
        $order->setId($id)
            ->setTotalPrice($this->getTotalPrice());

        $orderTotalPriceGross = [];

        foreach ($this->items as $item) {
            $orderItem = [
                'id'          => $item->getProduct()->getId(),
                'quantity'    => $item->getQuantity(),
                'total_price' => $item->getTotalPrice()
            ];

            foreach ($this->taxValues as $key => $value) {
                $orderItem[$key] = $item->getTotalPriceGross($value);
                $orderTotalPriceGross[$key] = $this->getTotalPriceGross($value);
            }

            $order->addItem($orderItem);
        }

        $order->setTotalPriceGross($orderTotalPriceGross);
        $this->items = [];
        $this->updatePrice();

        return $order;
    }

    /**
     * @param int $tax
     *
     * @return int
     */
    public function getTotalPriceGross(int $tax): int
    {
        $total = 0;
        $items = $this->getItems();

        foreach ($items as $item) {
            $total += $item->getTotalPriceGross($tax);
        }
        return $total;
    }

    /**
     * @param Item $item
     *
     * @return Cart
     */
    protected function addItem(Item $item): Cart
    {
        $this->items[] = $item;
        return $this;
    }
}

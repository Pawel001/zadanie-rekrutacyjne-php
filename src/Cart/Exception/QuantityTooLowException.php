<?php
/**
 * Created by PhpStorm.
 * User: Paweł Ledóchowski
 * Date: 05.07.18
 * Time: 09:31
 */

namespace Recruitment\Cart\Exception;

class QuantityTooLowException extends \Exception
{

}

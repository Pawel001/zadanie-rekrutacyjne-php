<?php

declare(strict_types=1);

namespace Recruitment\Tests\Cart;

use PHPUnit\Framework\TestCase;
use Recruitment\Cart\Item;
use Recruitment\Entity\Product;

class ItemTest extends TestCase
{
    /**
     * @test
     * @throws \Recruitment\Entity\Exception\InvalidUnitPriceException
     */
    public function itAcceptsConstructorArgumentsAndReturnsData(): void
    {
        $product = (new Product())->setUnitPrice(10000);

        $item = new Item($product, 10);

        $this->assertEquals($product, $item->getProduct());
        $this->assertEquals(10, $item->getQuantity());
        $this->assertEquals(100000, $item->getTotalPrice());
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function constructorThrowsExceptionWhenQuantityIsTooLow(): void
    {
        $product = (new Product())->setMinimumQuantity(10);

        new Item($product, 9);
    }

    /**
     * @test
     * @expectedException \Recruitment\Cart\Exception\QuantityTooLowException
     */
    public function itThrowsExceptionWhenSettingTooLowQuantity(): void
    {
        $product = (new Product())->setMinimumQuantity(10);

        $item = new Item($product, 10);
        $item->setQuantity(9);
    }

    /**
     * @throws \Recruitment\Cart\Exception\InvalidTaxValueException
     * @throws \Recruitment\Entity\Exception\InvalidUnitPriceException
     */
    public function testIfItReturnCorrectPriceWithDiffrentTax(): void
    {
        $product = (new Product())->setUnitPrice(10);
        $item = new Item($product, 5);

        $this->assertEquals(50, $item->getTotalPriceGross(0));
        $this->assertEquals(55, $item->getTotalPriceGross(5));
        $this->assertEquals(55, $item->getTotalPriceGross(8));
        $this->assertEquals(60, $item->getTotalPriceGross(23));
    }

    /**
     * @expectedException \Recruitment\Cart\Exception\InvalidTaxValueException
     * @throws \Recruitment\Entity\Exception\InvalidUnitPriceException
     */
    public function testIfItThrowsExceptionIfTaxValueIsIncorrect(): void
    {
        $product = (new Product())->setUnitPrice(10);
        $item = new Item($product, 1);
        $item->getTotalPriceGross(-5);
    }
}

<?php

declare(strict_types=1);

namespace Recruitment\Tests\Entity;

use PHPUnit\Framework\TestCase;
use Recruitment\Entity\Product;

class ProductTest extends TestCase
{
    /**
     * @test
     *
     * @expectedException \Recruitment\Entity\Exception\InvalidUnitPriceException
     */
    public function itThrowsExceptionForInvalidUnitPrice(): void
    {
        $product = new Product();
        $product->setUnitPrice(0);
    }
    /**
     * @test
     *
     * @expectedException \InvalidArgumentException
     */
    public function itThrowsExceptionForInvalidMinimumQuantity(): void
    {
        $product = new Product();
        $product->setMinimumQuantity(0);
    }

    /**
     * @expectedException \Recruitment\Cart\Exception\InvalidTaxValueException
     * @throws \Exception
     */
    public function testIfItThrowsExceptionIfTaxValueIsNotSet(): void
    {
        $product = new Product();
        $product->setUnitPrice(100);
        $product->getUnitPriceGross();
    }

    /**
     * @expectedException \Recruitment\Cart\Exception\InvalidTaxValueException
     * @throws \Exception
     */
    public function testIfItThrowsExceptionForInvalidTaxValue(): void
    {
        $product = new Product();
        $product->setUnitPrice(100);
        $product->setTax(-1)->getUnitPriceGross();
    }

    /***
     * @throws \Exception
     */
    public function testIfItReturnCorrectGrossPrice(): void
    {
        $product = new Product();
        $product->setUnitPrice(100);
        $this->assertEquals(100, $product->setTax(0)->getUnitPriceGross());
        $this->assertEquals(105, $product->setTax(5)->getUnitPriceGross());
        $this->assertEquals(108, $product->setTax(8)->getUnitPriceGross());
        $this->assertEquals(123, $product->setTax(23)->getUnitPriceGross());
    }
}
